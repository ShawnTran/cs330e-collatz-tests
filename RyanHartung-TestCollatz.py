#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "10 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  10)
        self.assertEqual(j, 1)

    def test_read_3(self):
        s = "20 50\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  20)
        self.assertEqual(j, 50)
    
    def test_read_4(self):
        s = "7484 6825\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  7484)
        self.assertEqual(j, 6825)

    def test_read_5(self):
        s = "4377 38272\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  4377)
        self.assertEqual(j, 38272)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(7484, 6825)
        self.assertEqual(v, 257)

    def test_eval_6(self):
        v = collatz_eval(4377, 38272)
        self.assertEqual(v, 324)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 1, 1, 1)
        self.assertEqual(w.getvalue(), "1 1 1\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 31, 594, 144)
        self.assertEqual(w.getvalue(), "31 594 144\n")
    
    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 7484, 6825, 257)
        self.assertEqual(w.getvalue(), "7484 6825 257\n")

    def test_print_5(self):
        w = StringIO()
        collatz_print(w, 4377, 38272, 324)
        self.assertEqual(w.getvalue(), "4377 38272 324\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("1 10\n10 1\n776 820\n445 888\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n10 1 20\n776 820 135\n445 888 179\n")

    def test_solve_3(self):
        r = StringIO("72 766\n209 900\n538 634\n359 561\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "72 766 171\n209 900 179\n538 634 137\n359 561 142\n")
        
    def test_solve_4(self):
        r = StringIO("288 597\n72 766\n209 900\n577 746\n719 951\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "288 597 144\n72 766 171\n209 900 179\n577 746 171\n719 951 179\n")

    def test_solve_5(self):
        r = StringIO("4377 38272\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "4377 38272 324\n")
# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
