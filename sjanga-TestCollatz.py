#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
        
    # Added cases
    def test_read_2(self):
        s = "1 15\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 15)
        
    def test_read_3(self):
        s = "9999 10000\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  9999)
        self.assertEqual(j, 10000)
        
    def test_read_4(self):
        s = "999999 999900\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  999999)
        self.assertEqual(j, 999900)
        
    def test_read_5(self):
        s = "1 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 1)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)
        
    # Added Cases
    def test_eval_5(self):
        v = collatz_eval(1, 100000)
        self.assertEqual(v, 351)
        
    def test_eval_7(self):
        v = collatz_eval(999998, 999999)
        self.assertEqual(v, 259)
        
    def test_eval_8(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)
        
    def test_eval_9(self):
        v = collatz_eval(356, 356)
        self.assertEqual(v, 33)

    def test_eval_10(self):
        v = collatz_eval(123456, 331)
        self.assertEqual(v, 354)


    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
        
    # Added cases
    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 1, 1, 1)
        self.assertEqual(w.getvalue(), "1 1 1\n")
        
    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 100, 10, 3)
        self.assertEqual(w.getvalue(), "100 10 3\n")
        
    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 999945, 90, 8045)
        self.assertEqual(w.getvalue(), "999945 90 8045\n")
        
    def test_print_5(self):
        w = StringIO()
        collatz_print(w, 565, 3, 3)
        self.assertEqual(w.getvalue(), "565 3 3\n")
        
    def test_print_6(self):
        w = StringIO()
        collatz_print(w, 999999, 999999, 999999)
        self.assertEqual(w.getvalue(), "999999 999999 999999\n")
    

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
            
    # Added cases
    def test_solve_2(self):
        r = StringIO("1 1\n2 2\n3 3\n4 4\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n2 2 2\n3 3 8\n4 4 3\n")
            
    def test_solve_3(self):
        r = StringIO("842747 844169\n984263 984008\n999999 999999\n90000 80000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "842747 844169 388\n984263 984008 352\n999999 999999 259\n90000 80000 333\n")
            
    def test_solve_4(self):
        r = StringIO("25 20\n20 15\n15 10\n10 5\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "25 20 24\n20 15 21\n15 10 18\n10 5 20\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
