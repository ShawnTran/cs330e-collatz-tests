#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, collatz_range_helper


import random
# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "1 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 1)

    def test_read_3(self):
        s = "2 999999\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 2)
        self.assertEqual(j, 999999)

    def test_read_4(self):
        s = "101 202\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 101)
        self.assertEqual(j, 202)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(1, 99999)
        self.assertEqual(v, 351)

    def test_eval_6(self):
        v = collatz_eval(10, 1)
        self.assertEqual(v, 20)

    def test_eval_7(self):
        v = collatz_eval(2,2)
        self.assertEqual(v, 2)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 201, 210, 89)
        self.assertEqual(w.getvalue(), "201 210 89\n")


    # # -----
    # # collatz_range_helper
    # # -----

    def test_range_1(self):
        i = 5000
        j = 10000
        i_new, j_new, i_upper, j_lower = collatz_range_helper(i, j)
        self.assertEqual(i_new, 5)
        self.assertEqual(j_new, 10)
        self.assertEqual(i_upper, 5000)
        self.assertEqual(j_lower, 10001)

    def test_range_2(self):
        i = 1
        j = 1
        i_new, j_new, i_upper, j_lower = collatz_range_helper(i, j)
        self.assertEqual(i_new, -1)
        self.assertEqual(j_new, -1)
        self.assertEqual(i_upper, -1)
        self.assertEqual(j_lower, -1)

    def test_range_3(self):
        i = 999
        j = 9999
        i_new, j_new, i_upper, j_lower = collatz_range_helper(i, j)
        self.assertEqual(i_new, 1)
        self.assertEqual(j_new, 9)
        self.assertEqual(i_upper, 1000)
        self.assertEqual(j_lower, 9001)
    # # -----
    # # solve
    # # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

        
    def test_solve_2(self):
        r = StringIO("1 99999\n10 1\n2 2\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 99999 351\n10 1 20\n2 2 2\n")
        
    def test_solve_3(self):
        r = StringIO("1 1\n2 3\n1 10101\n100 1\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n2 3 8\n1 10101 262\n100 1 119\n")
        
    def test_solve4(self):
        r = StringIO("2 20\n200 300\n301 310\n1000 2000\n 1 999999\n5000 10000\n")
        w = StringIO() 
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "2 20 21\n200 300 128\n301 310 87\n1000 2000 182\n1 999999 525\n5000 10000 262\n")    
    
    def test_solve5(self):
        r = StringIO("1 1\n1 999\n1000 1000\n999 1000\n 999 999\n 1000 999\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n1 999 179\n1000 1000 112\n999 1000 112\n999 999 50\n1000 999 112\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
